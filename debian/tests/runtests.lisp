(require "asdf")

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP")) ; Store FASL in some temporary dir
      (*default-pathname-defaults* (truename "test/")))
  (load "runall.lisp"))
